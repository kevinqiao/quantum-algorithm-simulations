import math
import numpy as np
import pandas as pd

np.set_printoptions(threshold=np.inf)
H = np.array([[1/math.sqrt(2), 1/math.sqrt(2)], [1/math.sqrt(2), -1/math.sqrt(2)]])

#A helper for U1
def helper(v, tree, s):
    state = np.zeros(2**s)
    for c in tree[v]:
        state = np.add(state, compState(int(c), 2**s))
    return state/math.sqrt(len(tree[v]))

#Generates the U_1 unitary
def U1(s, tree):
    ctr = 0
    for v in range(len(tree)):
        if len(tree[v]) != 0:
            matrix=np.kron(compState(v,len(tree)), helper(v, tree, s)).reshape(-1,1)
            base = np.kron(compState(v,len(tree)), compState(0, 2**s)).reshape(-1,1)
            ctr = v
            break

    for v in range(ctr+1, len(tree)):
        if len(tree[v]) != 0:
            newCol = np.kron(compState(v,len(tree)), helper(v, tree, s)).reshape(-1,1)

            matrix = np.append(matrix, newCol, axis=1)
            base = np.append(base,np.kron(compState(v,len(tree)), compState(0, 2**s)).reshape(-1,1),axis=1)
    copyMatrx = np.copy(matrix)
    for c in range(2**s):
        for v in range(len(tree)):
            if (c == 0 and len(tree[v]) == 0) or (c != 0 and len(tree[v]) == 0):

                newCol = np.kron(compState(v,len(tree)), np.ones(2**s)).reshape(-1,1)
                matrix = np.append(matrix, newCol, axis=1)
                base = np.append(base,np.kron(compState(v,len(tree)), compState(c, 2**s)).reshape(-1,1),axis=1)
            elif (c != 0 and len(tree[v]) != 0):

                newCol = np.kron(compState(v,len(tree)), helper(v, tree, s)).reshape(-1,1)
                matrix = np.append(matrix, newCol, axis=1)
                base = np.append(base,np.kron(compState(v,len(tree)), compState(c, 2**s)).reshape(-1,1),axis=1)
        matrix,r = np.linalg.qr(matrix)

    matrix,r = np.linalg.qr(matrix)
    for i in range(len(copyMatrx[0])):
        matrix[:,i] = copyMatrx[:,i]
    return np.matmul(matrix, base.T)

#Generates the O_x' unitary
def Ox(s, tree, lfx):
    treeDepth = math.log(len(tree)+1,2)
    matrix = np.kron(compState(0, len(tree)), compState(0, 2**s)).reshape(-1,1)
    base = np.kron(compState(0, len(tree)), compState(0, 2**s)).reshape(-1,1)
    for v in range(len(tree)):

        if len(tree[v]) != 0:
            xi = 2*v+1
            while 2*xi+2<len(tree):
                xi = 2*xi+2

            if xi >= lfx:
                xi = 1
            else:
                xi = 0

            for i in range(2**s):
                base = np.append(base, np.kron(compState(v, len(tree)), compState(i, 2**s)).reshape(-1,1), axis=1)
            if len(tree[v]) != 0 and len(tree[math.floor((v-1)/2)]) == 0:#boundary

                col = np.kron(compState(v, len(tree)), compState(xi, 2**s)).reshape(-1,1)
                for i in range(2**s):
                    matrix = np.append(matrix, col, axis=1)

            else:#non boundary
                col = (-1)**xi*np.kron(compState(v, len(tree)), compState(0, 2**s)).reshape(-1,1)

                for i in range(2**s):
                    matrix = np.append(matrix, col, axis=1)


    matrix = np.delete(matrix, 0, 1)
    base = np.delete(base, 0, 1)
    newMatrix = np.copy(matrix)
    while(len(matrix) != len(matrix[0])):
        matrix = np.append(matrix, np.kron(compState(0, len(tree)), compState(0, 2**s)).reshape(-1,1), axis=1)
        base = np.append(base, np.kron(compState(0, len(tree)), compState(0, 2**s)).reshape(-1,1), axis=1)

    q,r = np.linalg.qr(matrix)
    for i in range(len(newMatrix[0])):
        q[:,i] = newMatrix[:,i]

    return np.matmul(matrix, base.T)

#Generates the phi state required in U_2
def phi(u, tree):

    leaves = []
    q = [u]
    while len(q) != 0:

        node = q.pop(0)

        if 2*node+2 < len(tree):
            q.append(2*node+2)
        if 2*node+1 < len(tree):
            q.append(2*node+1)
        elif node < len(tree):
            leaves.append(node)
    ret = np.zeros(len(tree))

    for l in leaves:

        dif = math.floor(math.log(u+1,2)) - math.floor(math.log(l+1,2))
        ret = np.add(ret, compState(l, len(tree))/math.sqrt(2**(abs(dif))))

    return ret

#Generates the U2 unitary
def U2(s, tree):
    matrix = np.kron(compState(0, len(tree)), compState(0, 2**s)).reshape(-1,1)
    base = np.kron(compState(0, len(tree)), compState(0, 2**s)).reshape(-1,1)
    for v in range(len(tree)):

        if len(tree[v]) != 0:

            left = phi(2*v+1, tree)
            right = phi(2*v+2, tree)

            if len(tree[v]) != 0 and len(tree[math.floor((v-1)/2)]) == 0:#boundary
                base = np.append(base, np.kron(compState(v, len(tree)), compState(0, 2**s)).reshape(-1,1), axis=1)
                base = np.append(base, np.kron(compState(v, len(tree)), compState(1, 2**s)).reshape(-1,1), axis=1)
                colL = np.kron(right, compState(0, 2**s)).reshape(-1,1)
                colR = np.kron(left, compState(0, 2**s)).reshape(-1,1)
                matrix = np.append(matrix, colL, axis=1)
                matrix = np.append(matrix, colR, axis=1)

            elif len(tree[v]) != 0:#non boundary
                base = np.append(base, np.kron(compState(v, len(tree)), compState(0, 2**s)).reshape(-1,1), axis=1)
                base = np.append(base, np.kron(compState(v, len(tree)), compState(1, 2**s)).reshape(-1,1), axis=1)
                col = np.kron((right-left)/math.sqrt(2), compState(0, 2**s)).reshape(-1,1)
                matrix = np.append(matrix, col, axis=1)
                matrix = np.append(matrix, col, axis=1)

    matrix = np.delete(matrix, 0, 1)
    base = np.delete(base, 0, 1)

    while(len(matrix) != len(matrix[0])):
        matrix = np.append(matrix, np.kron(compState(0, len(tree)), compState(0, 2**s)).reshape(-1,1), axis=1)
        base = np.append(base, np.kron(compState(0, len(tree)), compState(0, 2**s)).reshape(-1,1), axis=1)

    matrix,r = np.linalg.qr(matrix)
    base,r = np.linalg.qr(base)

    return np.matmul(matrix, np.linalg.inv(base))

#Geneates a computational basis state
def compState(n,N):
    toRet = np.zeros(N)
    toRet[n] = 1
    return toRet

def numElements(n):
    if n == 1:
        return 1
    return n + numElements(n/2)

#Returns the index of the start of the level in tree
def getStartOfLevel(n):
    return 2**n-1

#Generates the required tree
def genTree(n=2):
    tree = ['']*int(numElements(2*(4**n)))
    for c in range(2**n):
        tree[getStartOfLevel(n)+c] += str(c)
    for c in range(2**n):
        for d in range(1,n+1):

            for i in range(int(2**(d-1))):
                row = getStartOfLevel(n)+(c+i+2**(d-1))%2**n

                for j in range(d):
                    row = 2*row+1

                for j in range(2**d):
                    tree[row+j] += str(c)
                T = c+2**(d)
    return tree

#Returns the leftmost left of tree rooted at n
def leftBound(n,d):
    if d == 0:
        return n;
    return leftBound(2*n+1,d-1)

#Returns the rightmost left of tree rooted at n
def rightBound(n,d):
    if d == 0:
        return n;
    return rightBound(2*n+2,d-1)

#Runs the algorithm described in Quantum Complexities Of Ordered Searching,Sorting, And Element Distinctness
def algo(x=0):

    tree = genTree(3)
    treeDepth = math.log(len(tree)+1,2)
    lfx = x + int(getStartOfLevel(treeDepth-1))
    N = int((len(tree)+1)/2)
    s = math.floor(math.log(N/2,4))
    stateSize = math.floor(math.log(len(tree),2)+1)

    V_c = {}
    for c in range(0, 2**s):
        V_c[c] = []
    for i in range(len(tree)):
        for j in range(len(tree[i])):
            V_c[int(tree[i][j])].append(i)

    state = np.zeros(len(tree)*2**s)

    for c in range(2**s):
        for v in V_c[c]:
            vDepth = math.floor(math.log(v+1,2))

            if lfx >= leftBound(v,treeDepth-vDepth-1) and lfx <= rightBound(v,treeDepth-vDepth-1):

                state = np.add(state, np.kron(compState(v,len(tree)),compState(c,2**s)))
                break

    state = state.reshape(-1,1)/math.sqrt(2**s)

    #Step 3
    t = time.time()
    U_1 = U1(s,tree)
    state = np.matmul(np.linalg.inv(U_1), state)

    #Step 4
    Oxp = Ox(s, tree, lfx)
    U_2 = U2(s, tree)
    A = np.matmul(U_2,Oxp)

    P = [lfx]
    c = lfx

    while c != 0:
        P.append(math.floor((c-1)/2))
        c = math.floor((c-1)/2)
    for p in P:
        lfxKet = np.kron(compState(lfx,len(tree)), compState(0,2**s))
        vKet = np.kron(compState(p,len(tree)), compState(0,2**s))
    state4 = np.matmul(A, state)

    return state4[lfx*(2**s)]
