import numpy as np
import math
import scipy.stats
from scipy.stats import unitary_group
import scipy.optimize as sc
import multiprocessing
from multiprocessing import Process, Array, Value
import sys

def delta(a,b):
    return 1 if a == b else 0

def f(j, x):
    if x<j:
        return -1
    else:
        return 1

def F(j, x, n):
    if x < n:
        return f(j, x)
    else:
        return -1 * f(j, x - n)

#mem = None

def Fhatv(n):
    g = np.vectorize(f)
    def Gpre(j, x):
        return g(j, x) if x < n else -1*g(j, x - n)
    G = np.vectorize(Gpre)
    def j_gen(a, b):
        return G(0,a)#*delta(a,b)
    j_genv = np.vectorize(j_gen)
    mem = np.fromfunction(j_genv, (2*n, 1), dtype=int)
    return mem

def T(x, j):
    return np.roll(x, j)


def Fhatv2(j, n):
    fnn = Fhatv(n)
    return T(fnn, j)

def j_gen(j, n, even):
    def j_fun(a,b):
        if a == j:
            return 1
        elif a == j+n:
            return 1 if even else -1
        else:
            return 0
    j_funv = np.vectorize(j_fun)
    return np.fromfunction(j_funv, (2*n,1), dtype = int) / math.sqrt(2.0)

# for p = 0 to 2n - 1
def momentum_state(p, n):
    def ecis(x):
        return (1.0/math.sqrt(2*n))*complex(math.cos(p*x*math.pi/n), math.sin(p*x*math.pi/n))
    ecis2 = np.vectorize(ecis)
    acc = np.fromfunction(ecis2, (2*n,), dtype=int)
    return np.reshape(acc, (2*n,1))


def val_find(c):
    if c != complex(0.0, 0.0):
        a = c.real
        b = c.imag
        return (a*a + b*b)/math.sqrt(a*a + b*b)
    else:
        return 0.0

exxe = lambda c : math.exp(c.real) * complex(math.cos(c.imag), math.sin(c.imag))

def val_find2(c):
    a = c.real
    b = c.imag
    x = math.atan2(-1.0*b, a) * complex(0.0, 1.0)
    return x

def fix_dot(v1, v2):
    return np.dot(v1.flatten(), np.conj(v2).flatten())

def measure(j, n, state, level):
    target = j_gen(j, n, level%2 == 0)
    temp = fix_dot(target, state)
    return temp.real**2 + temp.imag**2


n = int(sys.argv[1])
#n = 4096*4
base = max(2**int(math.log(n, 2) - 12), 1)

print("n = ", n)
print("base: ", base)

steps_view = True
j = 0
s = momentum_state(0,n)
#temp2 = np.zeros((12,2*n), dtype='complex128')
Fvm = Fhatv(n) # vector representation
level = 1

m_s = []
base_momentum_state = momentum_state(1,n)
m_s.append(momentum_state(0,n))
m_s.append(momentum_state(base,n))
#print(m_s[1])

k = base
while k < 2*n:
    m_s.append(math.sqrt(2*n)*np.multiply(m_s[len(m_s)-1],m_s[1],dtype='complex128'))
    #m_s[len(m_s)-1] = m_s[len(m_s)-1]/np.linalg.norm(m_s[len(m_s)-1])
    if(k%(n/4) == 0):
        print(k)
    k += base

def get_m_s(x):
    j_max = int(x/base)
    st = m_s[j_max]
    l_num = x%base
    k = 0
    while k < l_num:
        st = np.multiply(base_momentum_state, st, dtype='complex128')
        k += 1
    return st / np.linalg.norm(st)


for counter in range(10):
    psi0 = np.multiply(Fvm, s)
    psi1 = np.zeros((2*n,1), dtype='complex128')
    i = 0 if level%2 == 0 else 1
    while i < 2*n:
        dp = fix_dot(get_m_s(i), psi0)
        temp2 = val_find(dp)
        temp = get_m_s(i) * temp2
        psi1 = psi1 + temp
        i += 2
    print(measure(j, n, psi1, level))
    s = psi1
    level += 1